/* Copyright (C) 2012 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by the GPL v2 license that can
 * be found in the LICENSE file.
 *
 * Parts of this file are derived from the Linux kernel from the file with
 * the same name and path under include/.
 */
#ifndef VERITY_INCLUDE_LINUX_MODULE_H_
#define VERITY_INCLUDE_LINUX_MODULE_H_

#define EXPORT_SYMBOL(x)
#define MODULE_LICENSE(x)
#define MODULE_DESCRIPTION(x)
#define MODULE_ALIAS(x)

#define THIS_MODULE NULL

#endif  /* VERITY_INCLUDE_LINUX_MODULE_H_ */
